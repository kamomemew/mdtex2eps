#!/usr/bin/env python
#coding:utf-8
# mdtex2eps

"""
mdtex2eps.pyはmarkdownファイル内の数式をepsとして出力します。
設定ファイル(mdtex2eps.ini)を変更すれば
*使用するtexエンジン
*数式行の開始と終了の記号
*数式を埋め込むtexファイルの形式
*常にguiを使うかどうか
が変更できます。

! platexとdvipsにパスが通してください。
! 処理できるファイルはutf-8のみです。

mdtex2eps.py [オプション] [ファイル]

options
    -help     この文書を表示
    -stdout   該当する部分を表示し、画像は出力しない
    -reset    設定ファイルをリセットする
    -gui      GUIを有効に(試験的,おまけ程度)
"""
import re
import sys
import os
import codecs
import datetime
# Python2向け
try:
    import configparser
except ImportError:
    import ConfigParser as configparser    
try:
    import tkinter as tkinter
except ImportError:
    import Tkinter as tkinter
try:
    from tkinter import filedialog as tkFileDialog
except ImportError:
    import tkFileDialog
try:
    import tkinter as tkinter
except ImportError:
    import Tkinter as tkinter
try:
    from tkinter import messagebox as tkMessageBox
except ImportError:
    import tkMessageBox
root=tkinter.Tk()
root.withdraw()

# 変更しないでください
default_ini=u"""[SETUP]\n# 数式行の始まり\nstart_math = $$\n# 数式行の終わり\nend_math = $$\n# 使用するTeXエンジン\ntex = platex\n# 常にguiを使うか、使うならTrue\ngui = False\n\n[TEX]\n# 埋め込みに用いるTeXファイルの雛形、insert の部分が置き換えられる\n\n# !! style= より深くインデントしてください。 !!\nstyle=\n \\documentclass{jsarticle}\n \\pagestyle{empty}\n \\usepackage{amsmath}\n \\begin{document}\n \\[ insert \\]\n \\end{document}"""

conf = \
{
    'SETUP': 
        {
            'gui': False,
            'stdout': False
        },
    'TEX':{}
}
md_dir=""
md_file=""
md_txt=""

def print_info(str):
    if conf["SETUP"]["gui"]:
        tkMessageBox.showinfo("message",str)
    else :
        print(str)

def reset():
    if os.path.exists(os.path.dirname(os.path.abspath(__file__))+"/mdtex2eps.ini"):
        os.remove(os.path.dirname(os.path.abspath(__file__))+"/mdtex2eps.ini")
    print_info("configure reset.")
    config_init()

def config_init():
    parser=configparser.SafeConfigParser()
    if parser.read(os.path.split(os.path.abspath(__file__))[0]+"/mdtex2eps.ini"):
        for sect in parser.sections() :
            for opt in parser.options(sect) :
                #try:
                #    conf[sect][opt] = parser. getboolean(sect, opt)
                #except ValueError:
                #    print(sect,opt)
                #    conf[sect][opt] = parser.get(sect, opt)
                if parser.get(sect, opt).lower() == "true" or parser.get(sect, opt).lower() == "false":
                    conf[sect][opt] = parser. getboolean(sect, opt)
                else:
                    conf[sect][opt] = parser.get(sect, opt)
    else:
        f=codecs.open(os.path.split(os.path.abspath(__file__))[0]+"/mdtex2eps.ini","w","utf-8")
        f.write(default_ini)
        f.close()
        print_info("ini file is made.\nterminated.")
        exit(0)

def escape_st():
    def rp(s):
        """正規表現のエスケープを行う"""
        return \
        s.replace("\\","\\\\")\
        .replace("*","\*")   \
        .replace("+","\+")   \
        .replace(".","\.")   \
        .replace("?","\?")   \
        .replace("{","\{")   \
        .replace("(","\(")   \
        .replace("[","\[")   \
        .replace("^","\^")   \
        .replace("$","\$")   \
        .replace("-","\-")   \
        .replace("|","\|")   \

    conf["SETUP"]["start_math"]=rp(conf["SETUP"]["start_math"])
    conf["SETUP"]["end_math"]=rp(conf["SETUP"]["end_math"])

def argparse(arg_all):
    for arg in arg_all[1:]:
        arg=arg.replace('"',"").replace("'","")
        if arg == "-stdout" or arg == "--stdout":
            conf["SETUP"]["stdout"] = True
        elif arg == "h" or arg == "-h" or arg == "--help" or arg == "-help":
            print_info(__doc__)
            exit(0)
        elif arg == "reset" or arg == "-reset" or arg == "--reset":
            reset()
        elif arg == "gui" or arg =="-gui" or arg =="--gui":
            conf["SETUP"]["gui"] = True
        elif arg == "cui" or arg=="-cui" or arg=="--cui":
            conf["SETUP"]["gui"] = False
        else :
            if os.path.isfile(arg):
                global md_file
                md_file = os.path.abspath(arg)
            else :
                print_info(">  "+arg+"\n!! strange argument. abording.")
                exit(1)

def set_var():
    global md_dir,md_file,md_txt
    if not md_file:
        print_info("mdfile is not exist.")
        exit(1)
    md_dir=os.path.dirname(md_file)
    f=codecs.open(md_file,"r","utf-8")
    try :
        md_txt=f.read()
    except UnicodeDecodeError:
        print_info("This file is NOT utf-8.\nabording.")
        exit(1)
    f.close
    os.chdir(md_dir)

def load_file_gui():
    global md_file
    if not md_file:
        md_file = tkFileDialog.askopenfilename(filetypes = [('markdown', ('.md', '.mdml','.md')),('ALL', '.*')],initialdir = "./")
    set_var()

def chkengine():
    if os.system("which " +conf["SETUP"]["tex"].split(" ")[0]+ " >/dev/null 2>&1") != 0:
        print_info( conf["SETUP"]["tex"]+" is not available.")
        exit(1)
    if os.system("which dvips >/dev/null 2>&1") != 0:
        print_info("dvips is not available.")
        exit(1)

def tex_format_match():
    start=conf["SETUP"]["start_math"]    
    end=conf["SETUP"]["end_math"]    
    matched=re.findall(start+r"(.*?)"+end, md_txt, re.MULTILINE|re.DOTALL)    
    return matched

def convert_eps(matched):
    sty=conf["TEX"]["style"]
    platex=conf["SETUP"]["tex"]
    d = datetime.datetime.today()
    timestamp=d.strftime("%m%d-%H%M-%S_")
    for i in range(len(matched)):
        tex,dvi,eps,aux,log=timestamp+str(i)+".tex",timestamp+str(i)+".dvi",timestamp+str(i)+".eps",timestamp+str(i)+".aux",timestamp+str(i)+".log"
        #tex,dvi,eps,aux,log=str(i)+".tex",str(i)+".dvi",str(i)+".eps",str(i)+".aux",str(i)+".log"
        f=codecs.open(tex,"w","utf-8")
        #f.write(sty.replace("insert",matched[i].replace("\n","")))
        f.write(sty.replace("insert",matched[i]))
        f.close()
        os.system(platex +" -halt-on-error "+ tex)
        os.system("dvips -E -Ppdf "+dvi+" -o "+ eps)
        if os.path.exists(log):
            os.remove(log)
        if os.path.exists(dvi):
            os.remove(dvi)
        if os.path.exists(aux):
            os.remove(aux)

def print_eps(matched):
    s=""
    for i in range(len(matched)):
        s=s+matched[i].replace("\n","")+"\n"
    print_info(s)


config_init()
argparse(sys.argv)
if conf["SETUP"]["gui"]:
    load_file_gui()
else:
    set_var()
escape_st()
chkengine()
if not conf["SETUP"]["stdout"]:
    convert_eps(tex_format_match())
else:
    print_eps(tex_format_match())
